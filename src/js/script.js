window.addEventListener('load', function() {


    //  COMPTE À REBOURS DE L'ÉVÈNEMENT
    // Source du code : https://www.w3schools.com/howto/howto_js_countdown.asp
    // Si l'id 'days' existe
    if (document.getElementById('days')) {
        // On prépare la date
        let countdownDate = new Date("May 24, 2019 10:00:00").getTime();

        // On change le compteur à toutes les secondes
        let intervalTimer = setInterval(function () {

            // On va chercher la date et l'heure d'aujourd'hui
            let today = new Date().getTime();

            // On trouve la distance entre aujourd'hui et la date de l'évènement
            let distance = countdownDate - today;

            // Calcul pour les jours, heures, minutes et secondes
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // On apparaît le résultat à l'écran
            document.getElementById('days').innerHTML = days;
            document.getElementById('hours').innerHTML = hours;
            document.getElementById('minutes').innerHTML = minutes;
            document.getElementById('seconds').innerHTML = seconds;

            // Si le countdown est fini
            if (distance < 0) {
                // On arrête l'intervalle
                clearInterval(intervalTimer);

                // On affiche un texte qui mentionne que l'évènement est passé
                let section = document.querySelector('.section-heros aside')
                section.innerHTML = "L'evenement est fini !";
                section.style.color = '#fff';
                section.style.fontFamily = "'ostrich_sansblack', Impact, sans-serif"
                section.style.fontSize = '3.5rem';
            }
        }, 0);
    }


    /* ----------------------------------------------------------------------------------- */

    // ONGLETS DE L'HORAIRE
    // Si on est bien sur la page de la programmation
    if (/programmation/.test(window.location.href)) {
        // On va chercher les sections des jours
        let jour1 = document.getElementById('jour-1');
        let jour2 = document.getElementById('jour-2');
        let jour3 = document.getElementById('jour-3');

        // On va chercher les boutons des dates
        let btnDate1 = document.getElementById('btn-date-1');
        let btnDate2 = document.getElementById('btn-date-2');
        let btnDate3 = document.getElementById('btn-date-3');

        // Ajout de l'évènement clic sur le bouton
        btnDate1.addEventListener('click', (e) => {
            // On affiche l'horaire du jour 1 et on cache les autres
            jour1.style.display = "block";
            jour2.style.display = "none";
            jour3.style.display = "none";

            // Ajout de la classe jour-actif sur le bouton de la date sélectionnée
            if (!e.currentTarget.classList.contains('jour-actif')) {
                e.currentTarget.classList.add('jour-actif');
            }
            if (btnDate2.classList.contains('jour-actif')) {
                btnDate2.classList.remove('jour-actif');
            }
            else {
                btnDate3.classList.remove('jour-actif');
            }
        });

        // Ajout de l'évènement clic sur le bouton
        btnDate2.addEventListener('click', (e) => {
            // On affiche l'horaire du jour 2 et on cache les autres
            jour2.style.display = "block";
            jour1.style.display = "none";
            jour3.style.display = "none";

            // Ajout de la classe jour-actif sur le bouton de la date sélectionnée
            if (!e.currentTarget.classList.contains('jour-actif')) {
                e.currentTarget.classList.add('jour-actif');
            }
            if (btnDate1.classList.contains('jour-actif')) {
                btnDate1.classList.remove('jour-actif');
            }
            else {
                btnDate3.classList.remove('jour-actif');
            }
        });

        // Ajout de l'évènement clic sur le bouton
        btnDate3.addEventListener('click', (e) => {
            // On affiche l'horaire du jour 3 et on cache les autres
            jour3.style.display = "block";
            jour1.style.display = "none";
            jour2.style.display = "none";

            // Ajout de la classe jour-actif sur le bouton de la date sélectionnée
            if (!e.currentTarget.classList.contains('jour-actif')) {
                e.currentTarget.classList.add('jour-actif');
            }
            if (btnDate1.classList.contains('jour-actif')) {
                btnDate1.classList.remove('jour-actif');
            }
            else {
                btnDate2.classList.remove('jour-actif');
            }
        });
    }

});
